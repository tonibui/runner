package main

import (
	"encoding/json"
	"io/ioutil"
	"os/exec"
	"syscall"
)

// build command: go build -o bin -ldflags -H=windowsgui runner.go

var CONFIG_FILE_NAME = "runner.config.json"

type Config struct {
	Shell     string   `json:shell`
	ShellArgs []string `json:"shell.args"`
}

func readFile(fileName string) ([]byte, error) {
	return ioutil.ReadFile(fileName)
}

func loadJson(fileContent []byte, v interface{}) error {
	return json.Unmarshal(fileContent, v)
}

func loadConfigFile() Config {
	fileContent, err := readFile(CONFIG_FILE_NAME)
	if err != nil {
		panic("Cannot load config file")
	}
	config := Config{}
	loadJson(fileContent, &config)
	return config
}

func execute(name string, args ...string) {
	cmd := exec.Command(name, args...)
	cmd.SysProcAttr = &syscall.SysProcAttr {
		HideWindow: true
	}
	err := cmd.Start()
	if err != nil {
		panic("failed")
	}
}

func main() {
	config := loadConfigFile()
	execute(config.Shell, config.ShellArgs...)
}
